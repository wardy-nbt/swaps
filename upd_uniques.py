"""Update unique beers."""
from __future__ import annotations

import os
import sys
import time
from datetime import datetime
from typing import Final, Sequence

import pandas as pd
import requests
import requests_cache
import tenacity
from tenacity.retry import retry_if_exception_type
from tenacity.wait import wait_exponential

from db_uniques import HEADING, load_db, save_db
from model_uniques import Uniques

MINUTE: Final[int] = 60
HOUR: Final[int] = 60 * MINUTE
DAY: Final[int] = 24 * HOUR

CACHE_EXPIRY: Final[int] = HOUR / 4


ACCESS: Final[str] = os.environ["UNTAPPD_ACCESS_TOKEN"]
BASE: Final[str] = "https://api.untappd.com"
session = requests_cache.CachedSession("untappd", expire_after=CACHE_EXPIRY)
session.headers.update({"Authorization": f"Bearer {ACCESS}"})


USERS: Final[tuple[str, ...]] = (
    "coreymch",
    "mw1414",
    "ThirstyOClock",
    "fadell",
    "dapitz",
    "ThirstyOClock",
)


def main(users: Sequence[str] = None) -> pd.DataFrame:
    """Update the uniques database for specified users.

    Args:
        users (Sequence[str]): update the uniques for whom. Defaults to USERS.

    Returns:
        pd.DataFrame: new, updated unique database
    """
    if not users:
        users = USERS

    database = load_db()

    for user in users:
        print(f"\nupdating uniques for {user=}")
        user = user.casefold()
        database = update_uniques(database, user=user)

    save_db(database)

    print("\n", __name__, "updates complete")

    return database


def update_uniques(database: pd.DataFrame, user: str) -> pd.DataFrame:
    """Return db, updated for new uniques for user by querying Untappd API.

    Args:
        database (pd.DataFrame): current unique info
        user (str): user to update for

    Returns:
        pd.DataFrame: updated database
    """
    url = None  # initially just get most recent beers. Later, use pagination data
    while True:
        uniques = get_unique_page(user, url=url)
        uniques_df = build_uniques_df(user, uniques)

        # Get a union of known and new uniques
        old_uniques = len(database)
        database = pd.concat([database, uniques_df])
        database = database[~database.index.duplicated()]
        new_uniques = len(database) - old_uniques

        print(f"added {new_uniques} for {user=}")

        if new_uniques < uniques.response.beers.count:
            print(
                "not all beers on page were unique, so not loading more: "
                + f"{old_uniques=} {new_uniques=} "
                + f"{uniques.response.beers.count=} {url=}"
            )
            break  # the whole page wasn't unique. We're done

        url = uniques.response.pagination.next_url
        if not url:
            break  # no more pages

    return database


def get_unique_page(user: str, url: str | None = None) -> Uniques:
    """Fetch a page of uniques for a user.

    Args:
        user (str): user to fetch for
        url (str): URL to get. Defaults is to get user's last 50.

    Returns:
        Uniques: unique beers for user
    """
    if url:
        resp = _get(url)
    else:
        resp = _get(BASE + f"/v4/user/beers/{user}", http_params={"limit": 50})

    return Uniques(**resp.json())


def build_uniques_df(user: str, uniques: Uniques) -> pd.DataFrame:
    """Build a dataframe from an API response.

    Args:
        user (str): user the API call was made against
        uniques (Uniques): API response

    Returns:
        pd.DataFrame: dataframe of uniques
    """
    beer_info = extract_beer_info(user, uniques)
    return build_uniques_df_from_dict(beer_info)


def extract_beer_info(user: str, uniques: Uniques) -> list[dict[str, str | int]]:
    """Extract interesting info from response to unique API call.

    Args:
        user (str): user of uniques
        uniques (Uniques): response from API call

    Returns:
        list[dict]: items' interesting fields
    """
    return [
        {
            HEADING.bid: unique.beer.bid,
            HEADING.user: user,
            HEADING.beer_name: unique.beer.beer_name,
            HEADING.brewery_name: unique.brewery.brewery_name,
            HEADING.beer_style: unique.beer.beer_style,
            HEADING.first_had: unique.first_had,
            HEADING.times_had: unique.count,
            HEADING.rating_score: unique.beer.rating_score,
        }
        for unique in uniques.response.beers.items
    ]


def build_uniques_df_from_dict(beer_info: dict) -> pd.DataFrame:
    """Build a dataframe from extracted beer info.

    Args:
        beer_info (dict): beer info

    Returns:
        pd.DataFrame: dataframe of uiques
    """
    df = pd.DataFrame.from_records(beer_info, index=HEADING.csv_index)
    df[HEADING.first_had] = pd.to_datetime(df[HEADING.first_had])

    return df


# ----- API crap -----


def _log_retry(**kwargs) -> None:
    print(f"{datetime.now()}: retrying {kwargs=}")


@tenacity.retry(
    retry=retry_if_exception_type(requests.HTTPError),
    reraise=True,
    wait=wait_exponential(min=MINUTE / 4, max=MINUTE * 5),  # noqa: WPS432
    after=_log_retry,
)
def _get(url: str, http_params=None):
    print(f"getting {url=} ...")
    resp = session.get(url, params=http_params)
    print(
        f"got {resp.status_code=} "
        + f"{resp.headers['X-Ratelimit-Remaining']=} "
        + f"{resp.from_cache=}"
    )

    rate_limit = int(resp.headers["X-Ratelimit-Limit"])
    rate_remain = resp.headers["X-Ratelimit-Remaining"]
    rate_percent: float = int(rate_remain) * 100 / rate_limit

    if rate_percent < 15 and not resp.from_cache:  # noqa: WPS432
        print(f"slowing down because of {rate_remain=} of {rate_limit=}")
        time.sleep(HOUR / rate_limit)

    resp.raise_for_status()

    return resp


if __name__ == "__main__":
    main(sys.argv[1:])
