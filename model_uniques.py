# generated by datamodel-codegen:
#   filename:  a.json
#   timestamp: 2021-08-29T13:20:40+00:00

from __future__ import annotations

from typing import List, Optional, Union

from pydantic import BaseModel


class ResponseTime(BaseModel):
    time: float
    measure: str


class InitTime(BaseModel):
    time: int
    measure: str


class Meta(BaseModel):
    code: int
    response_time: ResponseTime
    init_time: InitTime


class UnreadCount(BaseModel):
    comments: int
    toasts: int
    friends: int
    messages: int
    venues: int
    veunes: int
    others: int
    news: int


class Notifications(BaseModel):
    type: str
    unread_count: UnreadCount


class Dates(BaseModel):
    first_checkin_date: str
    start_date: bool
    end_date: bool
    tzOffset: str


class Beer(BaseModel):
    bid: int
    beer_name: str
    beer_label: str
    beer_abv: float
    beer_ibu: int
    beer_slug: str
    beer_style: str
    beer_description: str
    created_at: str
    rating_score: float
    rating_count: int


class Contact(BaseModel):
    twitter: str
    facebook: str
    instagram: str
    url: str


class Location(BaseModel):
    brewery_city: str
    brewery_state: str
    lat: float
    lng: float


class Brewery(BaseModel):
    brewery_id: int
    brewery_name: str
    brewery_slug: Optional[str]
    brewery_page_url: str
    brewery_type: str
    brewery_label: str
    country_name: str
    contact: Contact
    location: Location
    brewery_active: int


class Item(BaseModel):  # noqa: WPS110
    first_checkin_id: int
    first_created_at: str
    recent_checkin_id: int
    recent_created_at: str
    recent_created_at_timezone: str
    rating_score: float
    user_auth_rating_score: float
    first_had: str
    count: int
    beer: Beer
    brewery: Brewery


class Beers(BaseModel):
    count: int
    items: List[Item]  # noqa: WPS110
    sort_english: str
    sort_name: str


class Pagination(BaseModel):
    next_url: str
    offset: Optional[int] = None
    max_id: Union[bool, str]


class Response(BaseModel):
    total_count: int
    dates: Dates
    is_search: bool
    sort: bool
    type_id: bool
    country_id: bool
    brewery_id: bool
    rating_score: bool
    region_id: bool
    container_id: bool
    is_multi_type: bool
    beers: Beers
    sort_key: str
    sort_name: str
    pagination: Pagination


class Uniques(BaseModel):
    meta: Meta
    notifications: Notifications
    response: Response
