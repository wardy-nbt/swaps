"""Find out what people have in their fridges."""

from __future__ import annotations

import sys
import textwrap
from functools import partial
from typing import Final, Iterable, Sequence

import numpy as np
import pandas as pd
from tabulate import tabulate
from untappd_scraper.user import User, WebUserList

import upd_uniques
from db_uniques import has_had

pd.set_option("mode.chained_assignment", "raise")

HAS: Final[str] = "_has"
HAD: Final[str] = "_had"

BEER_LENGTH: Final[int] = 35  # How many chars of a beer name do we really need?
INTEREST_WIDTH: Final[int] = 80  # How wide to make the interested column in Excel

FRIDGE_EXCEL: Final[str] = "fridge.xlsx"

FRIDGE_USERS: Final[tuple[str, ...]] = (
    "coreymch",
    "dapitz",
    "gregcswanson",
    "jimmy-swings",
    "LightBeerking",
    "louistheloo",
    "mw1414",
    "TheVot",
    "ThirstyOClock",
    "virtualpete",
)


def main(users: Sequence[str] = None):
    """Check people's fridge list for swappable uniques.

    Args:
        users (Sequence[str], optional): update the fridges for whom. Defaults to None.
    """
    if not users:
        users = FRIDGE_USERS
    users = {user.casefold() for user in users}

    database = upd_uniques.main(users)
    print()

    print(f"loading fridge lists for {users} ...")
    fridge_lists = get_fridge_list(users)
    print("loaded")

    check_interest(fridge=fridge_lists, users=users, database=database)

    fridge_lists.to_csv("fridges.csv")  # for debugging

    report = format_for_report(fridge_lists)

    write_excel(report)

    print("\n\n\n")
    print(tabulate(report, headers="keys", showindex=False, tablefmt="grid"))


def get_fridge_list(users: Iterable[str]) -> pd.DataFrame:
    """Load everyone's fridge list(s).

    Args:
        users (Iterable[str]): users to get fridge list for

    Returns:
        pd.DataFrame: user, list -> fridge stock and beer info
    """
    fridge_data = []

    for user_name in users:
        user = User(user_name)
        fridges = user.lists_detail("fridge") + user.lists_detail("bought")

        for fridge in fridges:
            fridge_data.extend(fridge_beers(fridge))

    df = pd.DataFrame(fridge_data)
    df.sort_index(inplace=True)
    return df


def fridge_beers(userlist: WebUserList) -> list[dict]:
    """Build a list of interesting details from a user's list.

    Args:
        userlist (WebUserList): user's list

    Returns:
        list[dict]: all their fridge beers with some details
    """
    fridge_data = []

    for beer in sorted(userlist.beers, key=lambda x: x.name):
        username = userlist.username
        short_brewery = " ".join(beer.brewery.split()[:2])
        short_purch = (
            " ".join(beer.details.purchased_at.split()[:2])
            if beer.details.purchased_at
            else ""
        )
        abv = f"{beer.abv:3.1f} %" if beer.abv else None
        cost = (
            f"${beer.details.purchase_price:5.2f}"
            if beer.details.purchase_price
            else None
        )
        stars = f"{beer.global_rating:.1f}" if beer.global_rating else None

        fridge_data.append(
            {
                "user": username,
                "full_scrape": userlist.full_scrape,
                "list_name": userlist.name,
                "beer": beer.name[:BEER_LENGTH],
                "brewery": short_brewery,
                "style": beer.style.partition(" - ")[0],
                "abv": abv,
                "serving": beer.details.serving,
                "#": beer.details.quantity,
                "cost": cost,
                "added": beer.added.strftime("%d/%b/%y"),
                "beer_id": beer.beer_id,
                "stars": stars,
                "purch_from": short_purch,
                "note": "\n".join(textwrap.wrap(beer.details.beer_notes, width=30))
                if beer.details.beer_notes
                else None,
            }
        )

    return fridge_data


def check_interest(
    fridge: pd.DataFrame, database: pd.DataFrame, users: Sequence[str]
) -> None:
    """Update fridge list with interesting details.

    Args:
        fridge (pd.DataFrame): fridge list contents for people
        database (pd.DataFrame): everyone's uniques
        users (Sequence[str]): users to report on. They may not be in fridge lists
    """
    update_with_had(fridge, database=database, users=users)
    update_with_has(fridge, users=users)
    update_with_interest(fridge)


def update_with_had(
    df: pd.DataFrame, *, database: pd.DataFrame, users: Sequence[str]
) -> None:
    """Update user-had values.

    Args:
        df (pd.DataFrame): untappd user lists
        database (pd.DataFrame): uniques database
        users (Sequence[str]): users to report on. They may not be in fridge lists
    """
    for user in users:
        user_had = partial(has_had, user=user, database=database)
        df[user + HAD] = df.beer_id.apply(user_had)


def _has(beer_id: int, *, user: str, df: pd.DataFrame) -> bool:
    return ((df.user == user) & (df.beer_id == beer_id)).any()


def update_with_has(df: pd.DataFrame, users: Sequence[str]) -> None:
    """Add columns for users having the beer in their own fridge.

    Args:
        df (pd.DataFrame): untappd user lists
        users (Sequence[str]): users to report on. They may not be in fridge lists
    """
    for user in users:
        user_has = partial(_has, user=user, df=df)
        df[user + HAS] = df.beer_id.apply(user_has)


def swappable(row: pd.Series) -> str:
    """Determine if the person would swap this beer.

    Args:
        row (pd.Series): list beer details

    Returns:
        str: swap status
    """
    if row[row.user + HAD]:
        return "had"
    if row["#"] > 1:
        return "multi"
    return "no"


def interested(row: pd.Series) -> str:
    """Determine if the person would swap this beer.

    Args:
        row (pd.Series): list beer details

    Returns:
        str: swap status
    """
    interest: list[str] = []

    users: list[str] = [u.partition(HAD)[0] for u in row.index if u.endswith(HAD)]

    for user in users:
        if not row[user + HAD] and not row[user + HAS]:
            interest.append(user)

    return ", ".join(interest)


def user_interested(row: pd.Series, *, user: str) -> str:
    """Determine if user would be interested in this beer.

    Args:
        row (pd.Series): list beer details
        user (str): user that's looking

    Returns:
        str: user's interest in beer
    """
    if row.user == user:
        return None
    if row[user + HAD]:
        return None
    if row[user + HAS]:
        return "have"
    return "yes"


def update_with_interest(df: pd.DataFrame):
    """Make a report from fridge list.

    Args:
        df (pd.DataFrame): source data
    """
    df["swappable"] = df.apply(swappable, axis="columns")
    df["interested"] = df.apply(interested, axis="columns")


def format_for_report(df: pd.DataFrame) -> pd.DataFrame:
    """Extract only report columns and tidy up.

    Args:
        df (pd.DataFrame): full data

    Returns:
        pd.DataFrame: report data
    """
    any_interest = True  # df["interested"] != ""
    is_swappable = df["swappable"] != "no"

    # df["note"] = "\n".join(textwrap.wrap(df["note"], width=20))

    calc_cols = [had for had in df.columns if had.endswith(HAD)] + [
        has for has in df.columns if has.endswith(HAS)
    ]
    df.user = np.where(df.full_scrape, df.user, df.user + "+")
    drop_cols = calc_cols + ["swappable", "full_scrape", "beer_id"]
    # drop_cols = calc_cols + ["swappable", "list_name", "full_scrape", "beer_id"]
    return df[is_swappable & any_interest].drop(columns=drop_cols)


def write_excel(
    df: pd.DataFrame, filename: str = FRIDGE_EXCEL, sheet_name: str = "fridges"
) -> None:
    """Create an Excel report.

    Args:
        df (pd.DataFrame): data for report
        filename (str): file name to create. Defaults to FRIDGE_EXCEL.
        sheet_name (str): sheet name. Defaults to "fridges".
    """
    # Create a Pandas Excel writer using XlsxWriter as the engine.
    writer = pd.ExcelWriter(filename, engine="xlsxwriter")

    # amend df so it's better for Excel formatting TODO

    # Write the dataframe data to XlsxWriter. Turn off the default header and
    # index and skip one row to allow us to insert a user defined header.
    df.to_excel(writer, sheet_name=sheet_name, startrow=1, header=False, index=False)

    # Get the xlsxwriter workbook and worksheet objects.
    worksheet = writer.sheets[sheet_name]

    # Get the dimensions of the dataframe.
    (max_row, max_col) = df.shape

    # Create a list of column headers, to use in add_table().
    column_settings = [{"header": column} for column in df.columns]

    # Add the Excel table structure. Pandas will add the data.
    worksheet.add_table(0, 0, max_row, max_col - 1, {"columns": column_settings})

    # Make the columns wider for clarity.
    worksheet.set_column(max_col - 1, max_col - 1, INTEREST_WIDTH)

    # Close the Pandas Excel writer and output the Excel file.
    writer.save()


if __name__ == "__main__":
    main(sys.argv[1:])
