"""Uniques database functions."""
from __future__ import annotations

from pathlib import Path
from typing import Final, NamedTuple

import pandas as pd


# ----- Column names -----
class Heading(NamedTuple):
    """Heading names."""

    user: str = "user"
    bid: str = "bid"
    beer_name: str = "beer_name"
    brewery_name: str = "brewery_name"
    beer_style: str = "beer_style"
    first_had: str = "first_had"
    times_had: str = "times_had"
    rating_score: str = "rating"

    @property
    def csv_index(self) -> list[str]:
        """Return headings to use as an index.

        Returns:
            list[str]: headings to use as index
        """
        return [self.user, self.bid]

    @property
    def csv_data(self) -> list[str]:
        """Return headings to use as data.

        Returns:
            list[str]: headings to use as data
        """
        return [
            self.beer_name,
            self.brewery_name,
            self.beer_style,
            self.first_had,
            self.times_had,
        ]


HEADING = Heading()


# ----- DB name -----
DATABASE: Final[Path] = Path("./uniques.csv")

# ----- Persistence Functions -----


def load_db(filename: Path = DATABASE) -> pd.DataFrame:
    """Return the current database.

    Args:
        filename (Path): filename to load CSV data from

    Returns:
        pd.DataFrame: database so far, from CSV
    """
    try:
        df = pd.read_csv(
            filename, parse_dates=[HEADING.first_had], index_col=HEADING.csv_index
        )
    except FileNotFoundError:
        print(f"need to create {filename=}")
        df = pd.DataFrame(columns=HEADING.csv_index + HEADING.csv_data)
        df.set_index(keys=HEADING.csv_index, inplace=True)

    return df


def save_db(database: pd.DataFrame, filename: Path = DATABASE) -> None:
    """Save to the CSV database.

    Args:
        database (pd.DataFrame): updated unique data
        filename (Path): Where to Save. Defaults to DATABASE.
    """
    database.to_csv(filename)


# ----- Accessor Functions -----


def has_had(bid: int, *, user: str, database: pd.DataFrame) -> bool:
    """Determine if a user has had a beer.

    Args:
        bid (int): beer ID
        user (str): user to check
        database (pd.DataFrame): uniques database

    Returns:
        bool: have they had it?
    """
    return (user, bid) in database.index
