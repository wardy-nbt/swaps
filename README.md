# swaps

Check out people's fridge list for what they probably are happy to swap

Untappd supports two types of list: Wish list. And User List

People can create as many User Lists as they want.

The `fridges.py` script matches people's purchased (but not drunk) beers against others uniques.



## Setting up a Fridge list
To set up a fridge list on Untappd on your phone:
1. go to your profile (bottom right icon)
2. scroll down to "LISTS >"
3. press "NEW LIST". This will create a new list (amazing, hey)
4. make sure the word "fridge" is in the list name
5. ensure visibility is "public"
6. description is optional but be creative
7. ensure "Check-in to remove beer" is on (switch set to the right)

## Adding beers to a Fridge list
1. Scan your beer
2. press "ADD TO LIST"
3. click on the fridge list you want to add it to
4. fill in the details, especially "Quantity", if it isn't just a single.
5. Serving Style, Purchased from, cost are also in the report so they could be helpful
6. Notes are also reported. For example, a few of my "swappables" are Fathers' Day presents - so that's nice to know
7. hit Done. And you're done

## Checking in a Fridge beer
1. Find the beer as normal. You can also go to it via the Fridge list - skip to step with the `...`below if you did this
2. Scroll down and you'll see a section "YOUR LISTS" which will include the Fridge list the beer is in
3. Find the beer in the Fridge list
4. If you click on the beer, you can see the details you entered in the Fridge details, such as purchase place and cost. If you don't care, just click on the three dots `...` and select checkin. If you did care, click the three dots `...` on the beer detail page - same result.
5. The checkin will now be pre-filled with the details from the list, eg, Can & Mountain Culture
6. Add who you're with, where you're drinking, comments blah blah - same as normal
7. Now when you hit checkin you'll be presented with a screen prompting you to decrement the number you have, which will just outright delete it if it's the last one.
8. Press "CONFIRM"


## Notes
The scraper can only see 15 beers at a time. Thanks to a drunken Shoey suggestion, the script tries various methods to get at least 30.

**but** if possible, keep fridge lists under 30 by making several lists.

For example, if you buy yourself a mixed 6 pack of uniques at the bottlo, add them to a "Non Swaps Fridge" list. Then they won't use up the 30 in your other fridge list(s) but the script will know you own them and won't want to swap someone else's.

Other ways is to make fridge lists like quaffer / non-unique / common fridge ... whatever.
